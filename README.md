## WebCam Python Coffee: live track the level of coffee in your coffee pot!
## Goal
From a live camera view of my coffee pot (with a webcam or a RPi), measure its level (full, empty, or between), and send messages (Slack, mail, SMS, etc) whenever the coffee pot becomes empty.

Basically, I want to redo the historical experiment of [the Trojan Room coffee pot](https://en.wikipedia.org/wiki/Trojan_Room_coffee_pot), the hacking project which gave birth to the webcam (in 1991).

[![logo.jpg](logo.jpg)](logo.jpg)

----

## Simple approach
### Measure once, and interpolate live
My simple idea is that when taking a black and white picture of the coffee pot, coffee will be black and the background white.
So simply counting the ratio of black pixels on the picture should basically enough to interpolate the level of the coffee pot.

> - *But the pot is not a cylinder, its level is not linearly depending on its volume!* Of course. I don't care.
> - *But it will be very inaccurate!* Yep.
> - *But... that's dumb and useless!* Yep. Loving it.
> - *But... we could do so much better!* Then do it.

### No matter, I will try:
I will give a live level of coffee in a coffee pot, as a real value in [0, 1], simply measured from:

- a camera picture of the full pot,
- a camera picture of the empty pot,
- a live camera picture of the pot, and a linear interpolation between empty and full level.

### Communicate
Then connect to Slack, Emails and text messages, and simply send a message when the pot is empty!

- Use [my homemade `FreeSMS.py` script](https://bitbucket.org/lbesson/bin/src/master/FreeSMS.py) to send me a text message,
- Use a good script or API to send a mail : FIXME which one?
- Use official [Slack]() Python API to send a Slack message
- Use [sentry.io](https://sentry.io/) if all this is too complicated.

## Challenge
> Do this with the minimal amount of work and a minimal Python script.

----

## How to run the experiment ?
### requirements
*First*, install the [requirements](https://pip.pypa.io/en/latest/user_guide/#requirements-files) with [`pip`](https://pip.pypa.io/), for [Python 2](https://docs.python/2/) (not yet 100% compatible for [Python 3](https://docs.python/3/)):
```bash
pip install -r requirements.txt
```

- If needed, see [this guide for installing SimpleCV](https://github.com/sightmachine/simplecv#ubuntu-1204),
- Or [ask an issue here](https://bitbucket.org/lbesson/webcam-python-coffee/issues/new).

### Run the script
*Then*, it should be very straight forward to run my [experiment script `main.py`](main.py):
```bash
python main.py    # Be sure to use python2
```

### Or with a [`Makefile`](Makefile) ?
You can also use the provided [`Makefile`](Makefile) file to do this simply:
```bash
make install      # install the requirements
make test         # run the main.py script
```

It can be used to check [the quality of the code](logs/main_pylint_log.txt) with [pylint](https://www.pylint.org/):
```bash
make lint lint3   # check the code with pylint
```

----

## References
- [This page on CL Cambridge's website](https://www.cl.cam.ac.uk/coffee/qsf/coffee.html),
- [this thread on SoftwareRecs on StackExchange](https://softwarerecs.stackexchange.com/questions/18134/python-library-for-taking-camera-images).

----

## :scroll: License ? [![GitHub license](https://img.shields.io/github/license/Naereen/badges.svg)](https://bitbucket.org/lbesson/webcam-python-coffee/blob/master/LICENSE)
[MIT Licensed](https://lbesson.mit-license.org/) (file [LICENSE](LICENSE)).

© 2017 [Lilian Besson](https://bitbucket.org/lbesson/).

[![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-green.svg)](https://bitbucket.org/lbesson/webcam-python-coffee/commits)
[![Ask Me Anything !](https://img.shields.io/badge/Ask%20me-anything-1abc9c.svg)](https://bitbucket.org/lbesson/ama)
[![Analytics](https://ga-beacon.appspot.com/UA-38514290-17/bitbucket.org/lbesson/webcam-python-coffee/README.md?pixel)](https://bitbucket.org/lbesson/webcam-python-coffee/)
![PyPI implementation](https://img.shields.io/pypi/implementation/ansicolortags.svg)
![PyPI pyversions](https://img.shields.io/pypi/pyversions/ansicolortags.svg)
[![ForTheBadge uses-badges](http://ForTheBadge.com/images/badges/uses-badges.svg)](http://ForTheBadge.com)
[![ForTheBadge uses-git](http://ForTheBadge.com/images/badges/uses-git.svg)](https://bitbucket.org/)

[![forthebadge made-with-python](http://ForTheBadge.com/images/badges/made-with-python.svg)](https://www.python.org/)
[![ForTheBadge built-with-science](http://ForTheBadge.com/images/badges/built-with-science.svg)](https://bitbucket.org/lbesson/)
